Projects using Quick

Over ten-thousand apps use either Quick and Nimble however, as they are not included in the app binary, neither appear in “Top Used Libraries” blog posts. Therefore, it would be greatly appreciated to remind contributors that their efforts are valued by compiling a list of organizations and projects that use them.

Does your organization or project use Quick and Nimble? If yes, please add your project to the list.

Who uses Quick

Similar to projects using Quick, it would be nice to hear why people use Quick and Nimble. Are there features you love? Are there features that are just okay? Are there some features we have that no one uses?

Have something positive to say about Quick (or Nimble)? If yes, provide a testimonial here.

Privacy Statement

Quick is a library that is only used for testing and should never be included in the binary submitted to App Store Connect. Your app will be rejected if you do include Quick in the submitted binary because Quick uses private APIs to better integrate with Xcode.

Despite not being shipped to Apple, Quick does not and will never collect any kind of analytics or tracking.